#require the class for http client
require 'net/http'
#require the URI class
require 'uri'
#require the DateTime subclass
require 'date'
#requiring the json module for deserialising JSON objects
require 'json'

def viewRates(jsonObject, base)
    puts "Viewing rates for #{base}"
    #iterates through the hash and prints each key and value
    jsonObject.each do |currency, rate|
        puts "#{currency} - #{rate}"
    end
    puts "Press enter to back to the menu"
    gets.chomp
end

def printRates(jsonObject, base)
    #boolean variable
    writeFile = true
    #while loop to ensure that the user enters a valid location to write a text file
    while writeFile == true
        puts "Please specify where you want to write the file 'currencyRates.txt' to."
        #gets file path from user
        location = gets.chomp
        puts "Writing file, please wait..."
        begin
            #storing the current date and time in a variable
            date = DateTime.now
            #target variable holds information of where to write the file to, the 'w' argument means write new file
            target = open("#{location}" + '\\currencyRates.txt', "w")
            #calling the write method to write date/time and base currency information
            target.write("Currency exchange rate for #{base} - at #{date}\n")
            #iterates through the hash and for each key/value pair - write it to the file
            jsonObject.each {|key, value| target.write("#{key} - #{value}\n")}
            #close the file
            target.close
        #rescue statement to catch errors 
        rescue StandardError => e
            #prints the specific error - stored in variable 'e'.
            puts "Error! Info: #{e.message}"
            puts "Please try again!"
        else
            #boolean set to false
            writeFile = false
            #print the location of where the file was written to
            puts "Complete! File is written to #{location}"
            puts "Press enter to go back to the main menu"
            gets.chomp
        end 
    end
end

def swapMoney(jsonObject, base)
    #boolean set to true
    swapCheck = true
    #while loop to ensure that the user enters a valid integer and/or a valid currency
    while swapCheck == true
        puts "Please specify which currency you want to swap with (by typing in the three character code, such as GBP, USD or JPY)"
        choice = gets.chomp.upcase
        puts "You have selected #{choice}"
        puts "How much money (#{base}) do you want to exchange for #{choice}?"
        value = gets.chomp.to_i
        puts "Calculating how much #{choice} you will get for #{value}#{base}"
        #begin statement for calculating the cost
        begin
            #calculate the cost based on user input by multiplying the value by the requested currency
            totalValue = value * jsonObject["#{choice}"]
            #variable displays two decimals
            totalValue = '%.02f' % totalValue
        #rescue statement to catch any errors
        rescue StandardError => e
            #print any errors to the user
            puts "Error! Info: #{e.message}"
            puts "Please try again!"
        else
            #boolean set to false so loop can end
            swapCheck = false
            #confirming to the user how much money they get
            puts "You will receive #{totalValue}#{choice}"
            puts "Press enter to return to the main menu"
            gets.chomp 
        end 
    end
end


puts "Welcome to Josh Bank!"
#bool for checking if currency load is complete
loadCheck = true
while loadCheck == true
    puts "Please enter your base currency! (In the form of three characters - such as GBP, USD, JPY)"
    #converts user entry to uppercase
    base = gets.chomp.upcase
    puts "You have selected #{base}"
    puts "Press enter to load currencies"
    gets.chomp

    #error catching when performing get request
    begin
        #integer for counting currencies
        currencyCount = 0
        #output of get request stored in a variable to deserialise JSON
        jsonString = Net::HTTP.get(URI.parse('https://api.exchangeratesapi.io/latest?base=' + base))
        jsonString = JSON.parse(jsonString)['rates'].each do |currency, rate|
            currencyCount += 1
        end
    rescue StandardError => e
        #code to handle errors
        #logging the error to the console
        puts "Error! Info: #{e.message}"
        puts "Please try again!"
    else
        #if no errors, run this code
        puts "Complete! #{currencyCount} currencies have been loaded."
        #boolean set to false to end loop
        loadCheck = false
    end
end

#boolean for menu loop
menu = true
while menu == true
    puts "Welcome to the main menu! Enter the first letter of a menu choice!"
    puts "(V)iew rates for #{base}"
    puts "(W)rite rates to file"
    puts "(S)wap money for another currency"
    puts "(Q)uit"
    #converts user input to uppercase
    choice = gets.chomp.upcase
    #case statement based on what the user types
    case choice
    when 'V'
        viewRates(jsonString, base)
    when 'W'
        printRates(jsonString, base)
    when 'S'
        swapMoney(jsonString, base)
    when 'Q'
        menu = false
        puts "Press enter to exit"
        gets.chomp
    else
        #else statement for catching any other input
        puts "Error! Unknown command! Try again!"
    end
end

